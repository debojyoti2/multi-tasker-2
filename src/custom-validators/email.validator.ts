import { AbstractControl } from '@angular/forms';

export function ValidateEmail(control: AbstractControl) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(re.test(String(control.value).toLowerCase())) {
        return true // Email valid
    } else {
        return {invalid: true}  // Email not valid
    }
}