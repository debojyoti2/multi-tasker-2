import { Component } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { OneSignalPage } from '../tasks/one-signal/one-signal';
import { CloudinaryPage } from '../tasks/cloudinary/cloudinary';

@Component({
  selector: 'tasks-list',
  templateUrl: 'tasks-list.html'
})
export class TasksListPage {
  public tasks: Array<{
    title: string,
    page: any,
    icon: string
  }>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController
  ) {
    this.menuCtrl.enable(true, 'myMenu');
    this.initTaskList();
  }

  private initTaskList(): void {
    this.tasks = [{
      title: "One Signal",
      page: OneSignalPage,
      icon: "notifications-outline"
    }, {
      title: "Cloudinary",
      page: CloudinaryPage,
      icon: "images"
    }]
  }

  public navigateToTask(task): void {
    this.navCtrl.push(task);
  }
}
