import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public user: any;
  public authToken: string;
  public loginType: string;

  constructor(
    public navCtrl: NavController,
  ) {
    this.user = JSON.parse(localStorage.getItem('user'));
    this.authToken = localStorage.getItem('authToken');
    this.loginType = localStorage.getItem('loginType');
  }

}
