import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, Platform } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TasksListPage } from '../tasks-list/tasks-list';
import { GooglePlus } from '@ionic-native/google-plus';
import { AngularFireAuth } from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { ValidateEmail } from '../../custom-validators/email.validator';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-login-home',
  templateUrl: 'login-home.html',
})
export class LoginHomePage implements OnInit {

  public normalLoginForm: FormGroup;
  public error: boolean = false;
  public googleUser: any;
  public errorMsg;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    public gp: GooglePlus,
    public afAuth: AngularFireAuth,
    public platform: Platform,
    public fb: Facebook,
    public alertCtrl: AlertController
  ) {
    this.menuCtrl.close();
    this.menuCtrl.enable(false, 'myMenu');
  }

  ngOnInit() {
    this.initReactiveForm();
    this.showDemoCredentials();
  }

  ionViewCanEnter(): boolean {
    // If authToken is present, don't allow to view login page 
    if (!!localStorage.getItem('authToken')) {
      this.navCtrl.push(TasksListPage);
      return false;
    } else {
      return true;
    }
  }

  public googleLogin(): void {
    if (this.platform.is('cordova')) {
      this.nativeGoogleLogin();
    } else {
      this.webGoogleLogin();
    }
  }

  private nativeGoogleLogin() {
    this.gp.login({}).then(googleLoginResponse => {
      this.storeToken(googleLoginResponse, "google");
      this.storeUserData({
        email: googleLoginResponse.email,
        name: googleLoginResponse.displayName
      })
      this.showLoginSuccessAlert();
      this.navCtrl.setRoot(TasksListPage);
    }).catch(err => {
      this.errorMsg = err;
    });
  }

  async webGoogleLogin(): Promise<void> {
    const provider = new firebase.auth.GoogleAuthProvider();
    this.afAuth.auth.signInWithPopup(provider).then(googleLoginResponse => {
      this.storeToken(googleLoginResponse, "google");
      this.storeUserData({
        name: googleLoginResponse.user.displayName,
        email: googleLoginResponse.user.email
      })
      this.showLoginSuccessAlert();
      this.navCtrl.setRoot(TasksListPage);
    }).catch(error => console.log(error));
  }

  public fbLogin() {
    if (this.platform.is('cordova')) {
      this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
        this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
          let fbUser = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'], auth: response };
          // Store token
          this.storeToken(response, 'facebook');
          // Store user data
          this.storeUserData({
            email: fbUser.email,
            name: fbUser.first_name
          })
          this.showLoginSuccessAlert();
          this.navCtrl.setRoot(TasksListPage);
        });
      });
    } else {
      const alert = this.alertCtrl.create({
        title: 'Browser detected',
        subTitle: 'Please use android/ios to use this feature',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }

  public normalLogin(): void {
    if (!this.normalLoginForm.invalid) {
      // Demo data check
      if (this.normalLoginForm.value.email === 'john@gmail.com' && this.normalLoginForm.value.password === '123') {
        this.storeToken({'accessToken': 'sometoken'},'normal');
        this.storeUserData({
          email: 'john@gmail.com',
          name: 'John'
        })
        this.showLoginSuccessAlert();
        this.navCtrl.setRoot(TasksListPage);
      } else {
        this.error = true;
      }
    } else {
      return;
    }
  }

  private storeToken(token: any, type: string) {
    localStorage.setItem('authToken', JSON.stringify(token));
    localStorage.setItem('loginType', type)
  }

  private storeUserData(user): void {
    localStorage.setItem('user', JSON.stringify(user));
  }

  private showDemoCredentials() {
    setTimeout(() => {
      let toast = this.toastCtrl.create({
        message: 'Demo Credentials Email: john@gmail.com Pass: 123',
        duration: 10000,
        position: 'bottom'
      });

      toast.present();
    }, 1000);
  }

  private showLoginSuccessAlert() {
    const user = JSON.parse(localStorage.getItem('user'));
    const loginType = localStorage.getItem('loginType');
    const alert = this.alertCtrl.create({
      title: `Hey ${user.name}!`,
      subTitle: `You have successfully logged in using ${loginType} account`,
      buttons: ['Continue']
    });
    alert.present();
  }

  private initReactiveForm(): void {
    this.normalLoginForm = this.formBuilder.group({
      email: ['', ValidateEmail],
      password: ['', [Validators.required, Validators.minLength(1)]]
    });
  }

  get f() { return this.normalLoginForm.controls; }
}

