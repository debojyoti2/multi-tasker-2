import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { OneSignal } from '@ionic-native/onesignal';


@Component({
  selector: 'page-one-signal',
  templateUrl: 'one-signal.html',
})
export class OneSignalPage {

  public notifications: any[] = [];
  public currentNotification: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public oneSignal: OneSignal,
    public platform: Platform,
    public toastCtrl: ToastController,
    public _changeRef: ChangeDetectorRef,
  ) {
    this.syncLocalStorage('get');
    this.startListeningToOneSignal();
    if (this.notifications.length === 0) {
      this.showWelcomeToast();
    }
  }

  ngDoCheck() {
    this._changeRef.detectChanges();
  }

  private startListeningToOneSignal(): void {
    if (this.platform.is('cordova')) {
      this.oneSignal.inFocusDisplaying(0);
      this.oneSignal.startInit('a2ccad08-2b04-4a36-86b5-796321420174', '270476319754');
      this.oneSignal.handleNotificationReceived().subscribe((res) => {
        this.storeNewNotification(res);
      });
      this.oneSignal.handleNotificationOpened().subscribe((res) => {
        // do something when a notification is opened
      });
      this.oneSignal.endInit();
    }
  }

  private storeNewNotification(oneSignalResponse): void {
    this.notifications.unshift({
      'payload': oneSignalResponse.payload,
      "dateTime": new Date().toLocaleString()
    });
    this._changeRef.detectChanges();
    this.syncLocalStorage('update');
  }

  private syncLocalStorage(action: string): void {
    switch (action) {
      case 'update':
        localStorage.setItem('onesignal', JSON.stringify(this.notifications));
        break;
      case 'get':
        if (!!localStorage.getItem('onesignal')) {
          let storedNotifications = localStorage.getItem('onesignal');
          this.notifications = JSON.parse(storedNotifications);
        }
    }
  }

  private showWelcomeToast() {
    const toast = this.toastCtrl.create({
      message: 'Messages will appear here once recieved in this session',
      duration: 4000
    });
    toast.present();
  }


}
