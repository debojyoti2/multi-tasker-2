import { Component } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { cloudinaryConfig } from '../../../configurations/cloudinary.config';


@Component({
  selector: 'page-cloudinary',
  templateUrl: 'cloudinary.html',
})
export class CloudinaryPage {

  public uploader: FileUploader;
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  public currentFile: any;

  public images: any[] = [];

  constructor() {
    this.initCloudinaryUploader();
    this.syncLocalStorage('get');
  }

  private initCloudinaryUploader() {
    this.uploader = new FileUploader({ url: `https://api.cloudinary.com/v1_1/${cloudinaryConfig.cloud_name}/upload` });
    this.uploader.onBuildItemForm = (fileItem: any, form: FormData): any => {
      // Add Cloudinary's unsigned upload preset to the upload form
      form.append('upload_preset', cloudinaryConfig.upload_preset);
      // To disable CORS requests
      fileItem.withCredentials = false;
      return { fileItem, form };
    };
    this.uploader.onAfterAddingFile = (fileObj) => {
      this.currentFile = fileObj.file;
    }
    this.uploader.onSuccessItem = (fileItem, imageResponse) => {
      this.currentFile = null;
      this.images.unshift(JSON.parse(imageResponse));
      this.syncLocalStorage('update');
    }
  }

  private syncLocalStorage(action: string): void {
    switch(action) {
      case 'get':
        if (!!localStorage.getItem('cloudinary')) {
          this.images = JSON.parse(localStorage.getItem('cloudinary'));
        }
        break;
      case 'update':
        let strigifiedImages =  JSON.stringify(this.images);
        localStorage.setItem('cloudinary', strigifiedImages);
        break;
    }
  }

}
