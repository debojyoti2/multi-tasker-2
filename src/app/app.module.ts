import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { CloudinaryModule, CloudinaryConfiguration } from '@cloudinary/angular-5.x';
import * as Cloudinary  from 'cloudinary-core';
import { GooglePlus } from '@ionic-native/google-plus';
import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ReactiveFormsModule } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook';
import { FileUploadModule } from 'ng2-file-upload';
import { HttpClientModule } from '@angular/common/http';
import { OneSignal } from '@ionic-native/onesignal';

// Custom pages
import { TasksListPage } from '../pages/tasks-list/tasks-list';
import { LoginHomePage } from '../pages/login-home/login-home';
import { OneSignalPage } from '../pages/tasks/one-signal/one-signal';
import { CloudinaryPage } from '../pages/tasks/cloudinary/cloudinary';
import { ProfilePage } from '../pages/profile/profile';

// Configuration files
import { cloudinaryConfig } from '../configurations/cloudinary.config';
import { firebaseConfig } from '../configurations/firebase.config';

@NgModule({
  declarations: [
    MyApp,
    TasksListPage,
    LoginHomePage,
    CloudinaryPage,
    OneSignalPage,
    ProfilePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FileUploadModule,
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig), // <-- firebase here
    AngularFireAuthModule,
    CloudinaryModule.forRoot(Cloudinary, cloudinaryConfig as CloudinaryConfiguration)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TasksListPage,
    OneSignalPage,
    LoginHomePage,
    ProfilePage,
    CloudinaryPage
  ],
  providers: [
    GooglePlus,
    StatusBar,
    OneSignal,
    Facebook,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
