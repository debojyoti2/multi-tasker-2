import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TasksListPage } from '../pages/tasks-list/tasks-list';
import { LoginHomePage } from '../pages/login-home/login-home';
import { ProfilePage } from '../pages/profile/profile';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  public rootPage: any;
  public pages: Array<{ title: string, component: any }>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen
  ) {
    this.configurePagesForSideMenu();

    if (this.userLoggedIn()) {
      // If already logged in, go to Tasklist page (skip login)
      this.rootPage = TasksListPage;
    } else {
      this.rootPage = LoginHomePage;
    }

    this.initializeApp();
  }

  public navigateToPage(page): void {
    if (page.title === "Logout") {
      localStorage.clear();
    }
    this.nav.setRoot(page.component);
  }

  private initializeApp(): void {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  private configurePagesForSideMenu(): void {
    this.pages = [
      { title: 'Tasks List', component: TasksListPage },
      { title: 'Profile', component: ProfilePage },
      { title: 'Logout', component: LoginHomePage }
    ];
  }

  private userLoggedIn(): boolean {
    if (!!localStorage.getItem('authToken')) {
      return true;
    } else {
      return false;
    }
  }
}
